# TEST REPORT (TEST SCENARIO 02)

## RESULTS

```output

PLAY [encrypt-secrets] *********************************************************

TASK [encrypt-secrets | Ensuring temp directory exists at /etc/repoman/tempdir/] ***
[0;33mchanged: [localhost][0m

TASK [encrypt-secrets | Ensuring files are removed from /etc/repoman/tempdir/] ***

TASK [encrypt-secrets | Ensuring encrypted copy of secrets in /etc/repoman/secrets/unencrypted/ placed in /etc/repoman/tempdir/] ***
[0;33mchanged: [localhost] => (item=/etc/repoman/secrets/unencrypted/testsecret)[0m
[0;33mchanged: [localhost] => (item=/etc/repoman/secrets/unencrypted/testsecret.pub)[0m

TASK [encrypt-secrets | Ensuring encrypted directory exists at /etc/repoman/secrets/encrypted/] ***
[0;33mchanged: [localhost][0m

TASK [encrypt-secrets | Ensuring copy of secrets in /etc/repoman/tempdir/ placed in /etc/repoman/secrets/encrypted/] ***
[0;33mchanged: [localhost] => (item=/etc/repoman/tempdir/testsecret)[0m
[0;33mchanged: [localhost] => (item=/etc/repoman/tempdir/testsecret.pub)[0m

PLAY RECAP *********************************************************************
[0;33mlocalhost[0m                  : [0;32mok=4   [0m [0;33mchanged=4   [0m unreachable=0    failed=0    [0;36mskipped=1   [0m rescued=0    ignored=0   

1 items passed all tests:
   1 tests in /home/codz/projects/gitlab.com/dreamer-labs/repoman/repoman/tests/test_02/bashtests/cat.bashtest
1 tests in 1 items.
1 passed and 0 failed.
Test passed.
1 items passed all tests:
   1 tests in /home/codz/projects/gitlab.com/dreamer-labs/repoman/repoman/tests/test_02/bashtests/tree.bashtest
1 tests in 1 items.
1 passed and 0 failed.
Test passed.

[INFO] Test scenario 02 passed!
```

## GIT COMMIT INFO

68c9814 | feat: Add yamllint to dryrun and bump to GLF 3.5.X