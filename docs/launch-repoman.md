# activate (aka repoman-launcher)

A helper script for launching a local instance of repoman and performing test operations

## Overview

This script performs ALL local operations that repoman is designed to perform.

## Index

* [main](#main)
* [repoman_launcher_usage](#repoman_launcher_usage)
* [repoman_launcher_install](#repoman_launcher_install)
* [launcher_env_check](#launcher_env_check)
* [launcher_dep_check](#launcher_dep_check)
* [build_repoman_devel_target](#build_repoman_devel_target)
* [build_repoman_local_target](#build_repoman_local_target)
* [prepare_local_user_env](#prepare_local_user_env)
* [test_framework_dep_check](#test_framework_dep_check)
* [self_heal](#self_heal)
* [test_scenario_dep_check](#test_scenario_dep_check)
* [prepare_test_env](#prepare_test_env)
* [run_test](#run_test)
* [report_test_results](#report_test_results)
* [make_test_report_commit](#make_test_report_commit)

### main

This function is the entrypoint to the script.
It performs a repoman launcher operation specified by the user.
It will either drop the user into a shell inside the repoman-local or repoman-devel env, *or*
It will execute one or more test scenarios from the `tests/` directory.

#### Arguments

* **$1** (string): Is the launcher operation to be performed.

#### Exit codes

* **0**: If all functions called by main() are successful and return 0.
* **1**: Functions,including main(), drop a [FAIL] and return 1 on fatal errors; then script will exit 1

### repoman_launcher_usage

Usage messaging for repoman-launcher

### repoman_launcher_install

Installs the repoman-launcher alias and bash completition

_Function has no arguments._

#### Exit codes

* **1**: If install fails
* **0**: If install works

### launcher_env_check

This function ensures that the user is not executing the script as root or with sudo.
It also makes sure REPOMAN_TLD was set by the function.

_Function has no arguments._

#### Exit codes

* **0**: If user is non-root and REPOMAN_TLD is set.
* **1**: If user is root or effectively root (via sudo); also fails if REPOMAN_TLD not set.

### launcher_dep_check

This function performs a preliminary dependency check.
Any non-bash-builtin dep in the path required by script should be added to this function.

_Function has no arguments._

#### Exit codes

* **0**: If all listed deps are present in $PATH
* **1**: If any listed dep is not present in $PATH; script exits after checking all.

### build_repoman_devel_target

This function builds a repoman container using the `repoman_devel` target in the Dockerfile.
This function builds the target excluding any cached layers

_Function has no arguments._

### build_repoman_local_target

This function builds a repoman container using the `repoman_local` target in the Dockerfile.
This function builds the target including any cached layers

_Function has no arguments._

### prepare_local_user_env

This function creates (or checks for) required credentials and configs.
This function is only executed if a environment launch operation is requested.
Test operations handle their own credentials and only use creds inside their scenario dirs.

#### Exit codes

* **0**: If all configs and creds are present
* **1**: If any configs or creds are missing and cannot be generated

### test_framework_dep_check

This function performs a preliminary dependency check on test framework deps.
Any non-bash-builtin dep in the path required for testing should be added to this function.
This function also ensures the python environment is setup properly per the Pipfile and .python-version files.

_Function has no arguments._

#### Exit codes

* **0**: If all listed deps are present in $PATH
* **1**: If any listed dep is not present in $PATH; script exits after checking all.

### self_heal

This function autoformats, lints, and generates docs for this script (and others).
The autoformatter rules are configured with the .editorconfig in the tld of this repo.

#### Arguments

* **$0** (string): Tells the function the location of the script so each tool can parse it.

#### Exit codes

* **0**: If autoformatting, linting, and autodoc generation all pass.
* **1**: Once the first one fails. No warnings (return 1). Fatal errors (exit 1).

#### See also

* [https://github.com/reconquest/shdoc#features](#httpsgithubcomreconquestshdocfeatures)
* [https://github.com/koalaman/shellcheck#how-to-use](#httpsgithubcomkoalamanshellcheckhow-to-use)
* [https://github.com/mvdan/sh](#httpsgithubcommvdansh)
* [https://editorconfig.org/](#httpseditorconfigorg)

### test_scenario_dep_check

This function performs a test scenario dependency check.
Any non-bash-builtin test deps in the path should be added to a test_*/bashtests/deps.txt file.
This function is called on a per-test basis.
The "|| [[ -n "$dep ]]" code is there for instances where the deps file does not end in a new line.

#### Arguments

* 1 integer Is the test number to check the test deps file for

#### Exit codes

* **0**: If all listed test deps are present in $PATH
* **1**: If any listed test dep is not present in $PATH; script will 'return 1'.

#### See also

* [https://stackoverflow.com/questions/12916352/shell-script-read-missing-last-line/12919766#12919766](#httpsstackoverflowcomquestions12916352shell-script-read-missing-last-line1291976612919766)

### prepare_test_env

This function prepares the build environments used to run tests.
Each test scenario gets its own environment and configs.
Environments consist of the entry in a "test_env.yml" file in the "fixtures/" subdir of the test scenario directory.
They also consist of creds and other files that normal live outside the repoman-local container, but are used by it.
Configs come from the contents of "repoman-data/" inside the test scenario directory.
If the subdirs are not already present in the test_* dir, they will **not** be created.
This ensures required directories are created by the repoman subcommands that require them inside the container.

#### Arguments

* **$1** (integer): Is the number of the test scenario being prepared.

#### Exit codes

* **0**: If successful.
* **1**: If failed; exits, does not return.

### run_test

This function performs most of the logic and flow of the test framework.
It executes docker-compose once environments are prepped/reset.
It runs `bashtest` once docker-compose exits (if successful).
When bashtest failures occur, this function warns, but does not return until the remainder of bashtests run
This gives other tests a chance to run, instead of the script exiting on first test failure.

#### Arguments

* **$1** (integer): Is the test scenario number to be executed.

#### Exit codes

* **0**: If all bashtests pass for a given test scenario
* **1**: If any bashtests with a scenario fail, or test scenario is not found; returns, does not exit.

#### See also

* [https://github.com/pahaz/bashtest#typical-use-case](#httpsgithubcompahazbashtesttypical-use-case)
* [https://docs.docker.com/compose/](#httpsdocsdockercomcompose)

### report_test_results

This function prints a non-debugged version of the test results to a log file
This file is included in a test commit, immediately following the regular commit.

#### Arguments

* 1 integer The test scenario number

### make_test_report_commit

This function makes a commit that includes the test.log files
The test report is only run if there are no uncommited files present in the git repo.
This implies that the user recently committed and is ready to generate a test report.
Ideally, the final commit before a MR should include this test commit.

