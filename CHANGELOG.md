# [3.2.0](https://gitlab.com/dreamer-labs/repoman/repoman/compare/v3.1.0...v3.2.0) (2022-10-19)


### Features

* Add project and subgroup delete feature ([b8be334](https://gitlab.com/dreamer-labs/repoman/repoman/commit/b8be334))

# [3.1.0](https://gitlab.com/dreamer-labs/repoman/repoman/compare/v3.0.0...v3.1.0) (2022-10-17)


### Features

* Add ability to add new repos and groups ([070b9a9](https://gitlab.com/dreamer-labs/repoman/repoman/commit/070b9a9))

# [3.0.0](https://gitlab.com/dreamer-labs/repoman/repoman/compare/v2.0.2...v3.0.0) (2022-10-07)


### Features

* Updated all underlying repoman deps ([54d3d7e](https://gitlab.com/dreamer-labs/repoman/repoman/commit/54d3d7e))


### BREAKING CHANGES

* gitlabform is now at v3, which requires updates to the
repoman configuration file for repoman-data repositories. See gitlabform
docs for upgrade guidance.

## [2.0.2](https://gitlab.com/dreamer-labs/repoman/repoman/compare/v2.0.1...v2.0.2) (2022-03-03)


### Bug Fixes

* check for existing group in docker build ([a9890fc](https://gitlab.com/dreamer-labs/repoman/repoman/commit/a9890fc))
* Various bug fixes ([538a4b1](https://gitlab.com/dreamer-labs/repoman/repoman/commit/538a4b1))

## [2.0.1](https://gitlab.com/dreamer-labs/repoman/repoman/compare/v2.0.0...v2.0.1) (2021-10-27)


### Bug Fixes

* Add better debugging ([d14e8f5](https://gitlab.com/dreamer-labs/repoman/repoman/commit/d14e8f5))

# [2.0.0](https://gitlab.com/dreamer-labs/repoman/repoman/compare/v1.2.4...v2.0.0) (2021-10-25)


### Bug Fixes

* Adjust ci to only check for test logs on MRs ([1fed28c](https://gitlab.com/dreamer-labs/repoman/repoman/commit/1fed28c))


### Features

* Consolidate interfaces to single entrypoint ([4acf09a](https://gitlab.com/dreamer-labs/repoman/repoman/commit/4acf09a))


### BREAKING CHANGES

* Items above are breaking to end user interfaces

## [1.2.4](https://gitlab.com/dreamer-labs/repoman/repoman/compare/v1.2.3...v1.2.4) (2021-10-22)


### Bug Fixes

* Add bugfix, for real this time ([8cac077](https://gitlab.com/dreamer-labs/repoman/repoman/commit/8cac077))

## [1.2.3](https://gitlab.com/dreamer-labs/repoman/repoman/compare/v1.2.2...v1.2.3) (2021-10-22)


### Bug Fixes

* Add bugfix to prevent deploy-configs job fails ([a5d06d2](https://gitlab.com/dreamer-labs/repoman/repoman/commit/a5d06d2))

## [1.2.2](https://gitlab.com/dreamer-labs/repoman/repoman/compare/v1.2.1...v1.2.2) (2021-10-22)


### Bug Fixes

* Add bugfix to correct Gitlab MR URL string ([4f7dd81](https://gitlab.com/dreamer-labs/repoman/repoman/commit/4f7dd81))

## [1.2.1](https://gitlab.com/dreamer-labs/repoman/repoman/compare/v1.2.0...v1.2.1) (2021-10-22)


### Bug Fixes

* Add debug information to help troubleshoot MR URL ([2575812](https://gitlab.com/dreamer-labs/repoman/repoman/commit/2575812))

# [1.2.0](https://gitlab.com/dreamer-labs/repoman/repoman/compare/v1.1.0...v1.2.0) (2021-10-22)


### Features

* Add support for reporting dryrun results to MR discussion ([cb6d358](https://gitlab.com/dreamer-labs/repoman/repoman/commit/cb6d358))

# [1.1.0](https://gitlab.com/dreamer-labs/repoman/repoman/compare/v1.0.1...v1.1.0) (2021-10-21)


### Bug Fixes

* Add more testing and input validation ([c4762e4](https://gitlab.com/dreamer-labs/repoman/repoman/commit/c4762e4))


### Features

* Add support for custom CA certs ([f8f17b1](https://gitlab.com/dreamer-labs/repoman/repoman/commit/f8f17b1))

## [1.0.1](https://gitlab.com/dreamer-labs/repoman/repoman/compare/v1.0.0...v1.0.1) (2021-10-20)


### Bug Fixes

* Fix ansible vault password generation ([5f980a1](https://gitlab.com/dreamer-labs/repoman/repoman/commit/5f980a1))

# 1.0.0 (2021-10-20)


### Bug Fixes

* Add releaserc ([200cb82](https://gitlab.com/dreamer-labs/repoman/repoman/commit/200cb82))


### Features

* Add automated semantic releasing ([4bc28d2](https://gitlab.com/dreamer-labs/repoman/repoman/commit/4bc28d2))
* Add initial repoman features ([e089628](https://gitlab.com/dreamer-labs/repoman/repoman/commit/e089628))
* Build repoman-remote ([861b846](https://gitlab.com/dreamer-labs/repoman/repoman/commit/861b846))
