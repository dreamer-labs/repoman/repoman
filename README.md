# repoman

## Purpose

Repoman is a tool for managing your Gitlab organization's repository configurations-as-code.
Current, (and planned), features include:

- [x] Encrypt single-line secret variables for inclusion in deployment plans (using `ansible-vault`)
- [x] Generate configuration-as-code deployment plans (using `ansible-core`)
- [x] Dryrun deployment plans (and print the results to merge requests)
- [x] Deploy deployment plans (using `gitlabform`)
- [x] Decrypt secret variables from previous deployment plans stored in Gitlab (in a `repoman-data` repository)
- [x] Create new repositories and subgroups under existing parent groups/subgroups

- [ ] Automatically rotate the `ansible-vault` password used to encrypt/decrypt secrets (for increased security)
- [ ] Automatically rotate deployment keypairs (useful for pipeline tools like `semantic-release`)
- [ ] Update certain config settings using a manually-triggered pipeline, executable via GitlabCI web interface
- [ ] Switch to using the ansible `filetree` lookup so variables can be stored in a directory hierarchy

## Quickstart

This is a TL;DR version of the Feature Tour... for the impatient crowd.

- Make new gitlab group for testing, (named whatever you like), and a project called "repoman-quickstart" inside it
- Make sure the user you are using for testing has "Owner" privileges on all the top-level group it will be managing
- Generate a personal access token with "api" perms and place it in `~/.config/repoman/GITLAB_TOKEN` on your local machine
- Run the following commands **_after_** updating the value of `gitlab_group` below
- `export gitlab_group="put_your_gitlab_group_url_slug_here";`
- `git clone https://gitlab.com/dreamer-labs/repoman/repoman.git && \`
  `cd repoman/ && \`
  `./launch-repoman install && \`
  `source ~/.config/repoman/bashrc && \`
  `cp -pR example-repoman-data/ repoman-data/ && \`
  `sed -i "s%placeholder%$gitlab_group%g" ./repoman-data/configs/config.yml.j2 && \`
  `sed -i "s%placeholder%$gitlab_group%g" ./repoman-data/configs/vars.yml && \`
  `launch-repoman`
- Wait for the `repoman-local` environment to build and launch (takes approximately 1-2 minutes)
- `repoman encrypt-secrets` && \
  `repoman dryrun-deploy`
- Review the output of `repoman dryrun-deploy` (it shows the results of applying `repoman-data/tempdir/config.yml`)
- If everything looks right, and you'd like to apply those configs to your test group/repository...
- `repoman deploy-configs`
- `exit` to return to your normal shell
- Congrats on your first repository configuration-as-code deploy using `repoman`!

## Table of Contents

[[_TOC_]]

## Feature Tour

This is a more verbose version of the quickstart with a few extra steps thrown in to illustrate all the current features of `repoman`.

### Create Gitlab personal access token

In order to apply configuration-as-code to a repo, a personal access token is needed.

- On your local machine, open a web browser
- Login to <https://gitlab.com>
- Click your profile avatar/photo in the top, right-hand corner
- In the drop-down menu, click _Preferences_
- On the profile preferences screen, click _Access Tokens_
- On the access token screen, create a new token with "api" permissions (and ensure the user has "Owner" on the repos you plan to manage).
- Copy the token value to `~/.config/repoman/GITLAB_TOKEN` on your local machine
- Set the permissions to prevent access by other users (i.e. `chmod 0600`)

### Create a "repoman-quickstart" repo

In order to test repoman, you need a test organization and repo to apply configuration-as-code to.

- Login to <https://gitlab.com>
- On the homescreen, click _Menu_ in the top, left corner
- From the drop-down menu, select _Groups_ > _Create group_
- On the next screen, select _New Group_
- On the group creation screen, set `Group name` and `Group URL` to whatever you like
  - For the remaining fields, the default values are fine
- On the new group's main page, click the _New Project_ button
- On the next screen, select _Create a Blank Project_
- On the project creation screen, create a project under your user with the `repoman-quickstart` slug
- It should be under your new group, like this: <https://gitlab.com/yournewgroup/repoman-quickstart>

### Get `repoman`

- On your local machine, navigate to your preferred working directory, then clone `repoman`
- `git clone https://gitlab.com/dreamer-labs/repoman/repoman.git`
- `cd repoman/`

### Configure `repoman-data/` directory

Repoman requires some configuration-as-code in order to apply it to a Gitlab repository.
Luckily, some working sample data is already provided in this repository.

- `cp -pR example-repoman-data/ repoman-data/`
- Replace "your_test_group_here" with the group URL slug of your new test group in the `sed` commands below; then run each `sed`
  - Note: the slug is the tail end of the `Group URL`, which is the part after the last slash in: <https://gitlab.com/your_test_group_here> )
- `sed -i 's%placeholder%your_test_group_here%g' ./repoman-data/configs/config.yml.j2`
- `sed -i 's%placeholder%your_test_group_here%g' ./repoman-data/configs/vars.yml`

### Activate the `repoman-local` environment

Repoman can be used in GitlabCI pipelines (`repoman-remote`) to automate configuration-as-code deployments;
however, this section focuses on applying configuration-as-code from a local machine.

- `./launch-repoman install`
- Install any missing dependencies, and rerun `./launch-repoman install` (if needed)
- After successful installation, run `launch-repoman` to activate the `repoman-local` environment
- The `repoman-local` environment has a unique shell prompt, indicating that the environment has been entered

### Use `repoman-local` to encrypt a test secret

Prior to pushing secrets to a repository for safe keeping, the secrets must first be encrypted.
Repoman uses `ansible-vault` to encrypt secrets files.
A sample secrets file called "testsecret" is provided in the `repoman-data/secrets/unencrypted/` directory.
Inside the `repoman-local` environment, run the following command to encrypt all secrets files in this directory:

- `repoman encrypt-secrets`

Secrets will be encrypted using an `ansible-vault` password file that was auto-generated for you.
The password file is located in the `~/.config/repoman/ANSIBLE_VAULT_PASS` file. Keep it safe.
Secrets are not encrypted in place, the encrypted versions are placed in the `repoman-data/secrets/encrypted/` directory.

### Decrypting a secret

If you have the `ansible-vault` password used to encrypt a secret, that same password can be used to decrypt it.
If you are no longer in `repoman-local` environment, launch-repoman again now.

- `launch-repoman`

Then clear out the `secrets/unencrypted/` directory, (just for demonstration purposes); this is not needed in production.

- `rm /etc/repoman/secrets/unencrypted/*`

Then run the decrypt command inside the environment.

- `repoman decrypt-secrets`

If you previously ran the `repoman encrypt-secrets` command,
a decrypted copy of the `testsecret` should now be in the `/etc/repoman/secrets/unencrypted/` directory.

Note: If you accidentally decrypt (or encrypt) secrets when a file of the same name already exists in the target directory,
do not worry; a copy of the existing file is made with a timestamp appended to the filename and placed alongside the original.

### Generating configuration-as-code and executing a `dryrun-deploy` with `repoman-local`

- `launch-repoman`
- `repoman dryrun-deploy`
- `cat /etc/repoman/tempdir/config.yml`

Stdout inside the `repoman-local` env should now show output from the dryrun and the contents of the config file that was generated. When executed with `repoman-remote` inside a GitlabCI pipeline, this also sends the report the merge request. This feature is explained elsewhere in the Feature Tour.

### Deploying configuration-as-code with `repoman-local`

- `launch-repoman`
- `repoman deploy-configs`
- `cat /etc/repoman/tempdir/config.yml`

The stdout from this set of commands should look very similar to the previous one,
except the configs displayed are actually deployed.
### Pushing encrypted secrets to a remote `repoman-data` repository previously encrypted with `repoman-local`

In a shared Gitlab organization, a peer-review should be conducted before applying configurations.
This requires you to create a merge request that includes your local copy of the configs, along with the encrypted secrets.
If you have not done so already, create a repository named `repoman-data` alongside your organization's other repositories.
Next, create a commit containing your configs and **encrypted** secrets.
The `.gitignore` file that ships with the `example-repoman-data/` *should* exclude directories containing secrets;
however, it is **your responsibility** to make sure stray secrets do not make thier way into the commit history!

Once the merge request is accepted, ensure you upload the contents of `~/.config/repoman/ANSIBLE_VAULT_PASS` to a GitlabCI variable in your remote `repoman-data` repository called `ANSIBLE_VAULT_PASS`.

You will also need a valid gitlab token uploaded to the repository's variables under the name: `GITLAB_TOKEN`.
You should use a service account that is **ONLY** a member of the repositories you wish to manage with `repoman`.
This limits the damage in the event that someone accidentally merges in a bad config that runs against the `'*'` project.
This token needs the "api" perms checkbox enabled and the user must be an Owner on the repos/groups it manages.

Note: Both of these variables can be set under `Settings > CI/CD > Variables` in your `repoman-data` repository.

### Decrypting existing secrets from an existing `repoman-data` repository

If you are not the first person to encrypt secrets and place them in a `repoman-data` repository,
you will need a copy of the `ansible-vault` password file used by other maintainers to decrypt secrets they generated.
That secret would have to be fetched from the repository like so:

- Login to <https://gitlab.com/>
- Retreive the secret from the project's settings menu under Settings > CI/CD > Variables
- Find the `ANSIBLE_VAULT_PASS` variable's value and copy it to `~/.config/repoman/ANSIBLE_VAULT_PASS` on the local machine

After you have the password needed to decrypt the secrets, the secrets will need to be pulled down to your local machine.

- `cd  repoman`
- `git clone https://gitlab.com/yourusernamehere/repoman-data/`

Note: the `repoman` repository already includes `.gitignore` entries to ignore the contents of the `repoman-data` subdirectory.

Next, launch-repoman, and then execute the `repoman decrypt-secrets` command.

- `launch-repoman`
- `repoman decrypt-secrets`

The decrypted secrets should now be in the `/etc/repoman/secrets/unencrypted` directory.
The same files are visible directly on the local machine (outside of the `repoman-local` environment).

### Creating new projects and subgroups

The underlying application used to configure existing repositories or subgroups (`gitlabform`) does not currently create them.
To add this as a feature, `repoman` uses the python library/CLI `python-gitlab` and wraps it to create two subcommands:

- GITLAB_PARENT_ID=group_parent_id_of_subgroup_here GITLAB_SUBGROUP_NAME=new_subgroup_name_here repoman create-subgroup
- GITLAB_PARENT_ID=group_parent_id_of_project_here GITLAB_PROJECT_NAME=new_project_name_here repoman create-project

These two subcommands are one-off commands that are only used once to create new projects and subgroups.
Their underlying functions are not run as part of the `deploy-configs` subcommand.
These are non-destructive subcommands, meaning they are not capable of archiving or deleting subgroups or projects.
These subcommands can also be configured to run as part of a manually triggered pipeline using the `repoman-remote` container.
See example jobs in `.gitlab-ci.yml` file to one way to setup the jobs, if desired. 

### Deleting projects and subgroups

The underlying application used to configure existing repositories or subgroups (`gitlabform`) does not currently delete them.
To add this as a feature, `repoman` uses the python library/CLI `python-gitlab` and wraps it to create two subcommands:

- GITLAB_SUBGROUP_ID=group_id_of_subgroup_here GITLAB_SUBGROUP_PATH=relative_subgroup_path_here repoman delete-subgroup
- GITLAB_PROJECT_ID=project_id_of_project_here GITLAB_PROJECT_PATH=full_project_path_here repoman create-project

These two subcommands are one-off commands that are only used once to delete projects and subgroups.
Their underlying functions are not run as part of the `deploy-configs` subcommand.
These are *destructive* subcommands, meaning they *are* capable of permenantly deleting subgroups or projects.
These subcommands can also be configured to run as part of a manually triggered pipeline using the `repoman-remote` container.
See example jobs in `.gitlab-ci.yml` file to one way to setup the jobs, if desired. 

### Using `repoman` with custom CA-signed SSL certificates

If you plan to use `repoman-local`, (or `repoman-remote`), to manage repositories on a Gitlab server that uses HTTPS and SSL certificates that were signed by a non-default certificate authority, you will need to provide `repoman` with a CA bundle that includes any root, (and possibly intermediate), certificates used to sign the server's SSL certificate.

This CA bundle needs to be uploaded in PEM (plaintext) format in `repoman-data/ca-certs/`. Cert files should end in `.crt`. The `example-repoman-data/ca-certs/` directory in this repository includes the latest US DoD CA bundle as example data.

Any certificate files in your `repoman-data/ca-certs/` directory will be added to the CA trust inside the `repoman` environment just-in-time prior to executing any `repoman` subcommand. Any certificates added to this subdirectory later will be added to the CA trust alongside previously included ones next time a `repoman` subcommand is run. This eliminates the need to exit and launch-repoman each time a CA bundle is added to the `repoman-data/ca-certs/` directory.

### Using `repoman-remote` in GitlabCI pipelines

The `example-repoman-data` directory in this repository includes a sample `.gitlab-ci.yml` file for using `repoman-remote` to execute the `dryrun-deploy` and `deploy-configs` and several manually triggered, one-off subcommands in a GitlabCI pipeline. The jobs in this file are configured in such a way that the `dryrun-deploy` subcommand is functioned each time a merge request is made to your `repoman-data` repository. The `dryrun-deploy` subcommand will print a copy of its dryrun output to the pipeline stdout as well as to the comments section of the merge request. This can be used by approvers to determine whether or not they will approve your merge request.

Important note: the `dryrun-deploy` subcommand will print one report per item listed under `projects:` in `repoman-data/configs/vars.yml`. As a result, **work smartly!** Any chance you have to combine configs, and apply them at the group, (rather than individual project level), do it! This will limit the amount of dryrun reports that get sent to merge request comments per merge request.

`gitlabform` v3, (available as of `repoman` v3), allows for you to pass `ALL` or `ALL_DEFINED` rather than group or project names to run against. This is usally the right setting for most organizations.

## Debugging

Repoman includes a debug feature that can be used only when repoman is executed locally. This feature will toggle on verbosity on Ansible and Bash, resulting in the possibility of secrets being revealed, which is why it is not allowing to be executed inside the repoman-remote container image. To toggle on debugging, you must set a variable prior to executing a repoman subcommand inside the local repoman environment, like so:

```
REPOMAN_DEBUG=true repoman deploy-configs
```

