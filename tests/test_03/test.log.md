# TEST REPORT (TEST SCENARIO 03)

## RESULTS

```output

PLAY [deploy-configs] **********************************************************

TASK [deploy-configs | Including vars from /etc/repoman/configs/vars.yml] ******
[0;32mok: [localhost][0m

TASK [deploy-configs | Ensuring temp directory exists at /etc/repoman/tempdir/] ***
[0;33mchanged: [localhost][0m

TASK [deploy-configs | Ensuring files are removed from /etc/repoman/tempdir/] ***

TASK [deploy-configs | Ensuring rendered copy of config.yml.j2 in /etc/repoman/configs/ placed in /etc/repoman/tempdir/] ***
[0;33mchanged: [localhost][0m

TASK [{{ anisble_play_name }} | Including tasks to perform and print results of gitlabform dryrun] ***
[0;36mincluded: /etc/ansible/dryrun-deploy.yml for localhost => (item={'name': 'mglftg', 'deploy_key': True})[0m
[0;36mincluded: /etc/ansible/dryrun-deploy.yml for localhost => (item={'name': 'mglftg/sub/Subproject2'})[0m

TASK [deploy-configs | Attempting lint of config file containing project (mglftg) using yamllint] ***
[0;32mok: [localhost][0m

TASK [deploy-configs | Attempting to print stdout of lint for project (mglftg)] ***
[0;32mok: [localhost] => {[0m
[0;32m    "msg": [[0m
[0;32m        ""[0m
[0;32m    ][0m
[0;32m}[0m

TASK [deploy-configs | Attempting verbose dryrun of config changes to mglftg using gitlabform] ***
[0;32mok: [localhost][0m

TASK [deploy-configs | Attempting to print stdout of dryrun for project (mglftg)] ***
[0;32mok: [localhost] => {[0m
[0;32m    "msg": [[0m
[0;32m        "🏗 GitLabForm version: 3.5.0 = the latest stable 😊",[0m
[0;32m        "Reading config from file: /etc/repoman/tempdir/config.yml"[0m
[0;32m    ][0m
[0;32m}[0m

TASK [deploy-configs | Attempting to print stderr of dryrun for project (mglftg)] ***
[0;32mok: [localhost][0m

TASK [deploy-configs | Setting facts for Gitlab merge request comment task below (and possible debugging later)] ***
[0;32mok: [localhost][0m

TASK [deploy-configs | Attempting to report dryrun results to a Gitlab merge request comment] ***
[0;32mok: [localhost][0m

TASK [deploy-configs | Printing debug information for Gitlab merge request URL debugging] ***
[0;32mok: [localhost] => {[0m
[0;32m    "repoman_mr_url_dict": {[0m
[0;32m        "repoman_CI_API_V4_URL": "",[0m
[0;32m        "repoman_CI_COMMIT_SHA": "unknown",[0m
[0;32m        "repoman_CI_MERGE_REQUEST_IID": "",[0m
[0;32m        "repoman_CI_PROJECT_ID": "",[0m
[0;32m        "repoman_mr_url": "/projects//merge_requests//notes"[0m
[0;32m    }[0m
[0;32m}[0m

TASK [deploy-configs | Attempting lint of config file containing project (mglftg/sub/Subproject2) using yamllint] ***
[0;32mok: [localhost][0m

TASK [deploy-configs | Attempting to print stdout of lint for project (mglftg/sub/Subproject2)] ***
[0;32mok: [localhost] => {[0m
[0;32m    "msg": [[0m
[0;32m        ""[0m
[0;32m    ][0m
[0;32m}[0m

TASK [deploy-configs | Attempting verbose dryrun of config changes to mglftg/sub/Subproject2 using gitlabform] ***
[0;32mok: [localhost][0m

TASK [deploy-configs | Attempting to print stdout of dryrun for project (mglftg/sub/Subproject2)] ***
[0;32mok: [localhost] => {[0m
[0;32m    "msg": [[0m
[0;32m        "🏗 GitLabForm version: 3.5.0 = the latest stable 😊",[0m
[0;32m        "Reading config from file: /etc/repoman/tempdir/config.yml"[0m
[0;32m    ][0m
[0;32m}[0m

TASK [deploy-configs | Attempting to print stderr of dryrun for project (mglftg/sub/Subproject2)] ***
[0;32mok: [localhost][0m

TASK [deploy-configs | Setting facts for Gitlab merge request comment task below (and possible debugging later)] ***
[0;32mok: [localhost][0m

TASK [deploy-configs | Attempting to report dryrun results to a Gitlab merge request comment] ***
[0;32mok: [localhost][0m

TASK [deploy-configs | Printing debug information for Gitlab merge request URL debugging] ***
[0;32mok: [localhost] => {[0m
[0;32m    "repoman_mr_url_dict": {[0m
[0;32m        "repoman_CI_API_V4_URL": "",[0m
[0;32m        "repoman_CI_COMMIT_SHA": "unknown",[0m
[0;32m        "repoman_CI_MERGE_REQUEST_IID": "",[0m
[0;32m        "repoman_CI_PROJECT_ID": "",[0m
[0;32m        "repoman_mr_url": "/projects//merge_requests//notes"[0m
[0;32m    }[0m
[0;32m}[0m

TASK [deploy-configs | Applying configurations to configured repositories using gitlabform] ***
[0;36mskipping: [localhost] => (item=None) [0m
[0;36mskipping: [localhost] => (item=None) [0m

PLAY RECAP *********************************************************************
[0;33mlocalhost[0m                  : [0;32mok=21  [0m [0;33mchanged=2   [0m unreachable=0    failed=0    [0;36mskipped=2   [0m rescued=0    ignored=0   

1 items passed all tests:
   1 tests in /home/codz/projects/gitlab.com/dreamer-labs/repoman/repoman/tests/test_03/bashtests/md5sum.bashtest
1 tests in 1 items.
1 passed and 0 failed.
Test passed.

[INFO] Test scenario 03 passed!
```

## GIT COMMIT INFO

68c9814 | feat: Add yamllint to dryrun and bump to GLF 3.5.X