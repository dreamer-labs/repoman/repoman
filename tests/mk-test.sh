#!/usr/bin/env bash
#
# @name mk-test.sh
# @brief This helper scripts assist repoman developers in writing new bashtest tests
# @description
#    This script assists repoman developers with writing tests for `bashtest` to parse.
#    In addition to readable text, `bashtest` also check non-printable characters when running tests.
#    This can make writing and troubleshooting these tests very tedious and frustrating.
#    This script seeks to solve that problem by automatating the process.
#    As a developer, once you are happy with a repoman feature's behavior, use this to write your tests.
#    It should be used to call things like tree, md5sum, etc to check a desired end state after a repoman run.
# @see https://github.com/pahaz/bashtest#typical-use-case

# @description
#    This function is the entrypoint to the script.
#    It moves old bashtests to $2.bak to avoid overwriting existing tests.
#    It replaces it with a file at $2 of the same name that starts with: '$ ' on line 1.
#    It appends '$1\n' where $cmd is the command `bashtest` will run.
#    It then runs the command and appends the output to the file, starting on line 2.
#    The resulting test file should pass if `bashtest` is run using it as input later.
#    ***Warning***: This script will actually run the command passed to it as $1.
#    ***Warning***: This script uses the `eval` builtin, so be careful and check your inputs!
# @arg $1 string This string should be double-quoted, and should contain the command you'd like bashtest to run.
# @arg $2 string This arg should be the desired file path where the script will place your bashtest.
# @exitcode 0 If bashtest file generation is successful.
# @exitcode 1 If bashtest file generation (or any prelimnary steps) fail. Exits, does not 'return 1'.
# @example
#    ./mk-test.sh "echo 'test'" test_99/bashtests/test.bashtest
# @stdout
#    [INFO] Creating bashtest file:
#           /home/user/repoman/tests/test_99/bashtests/test.bashtest
#
#    [INFO] Evaluating command and capturing output in bashtest file...
#
#    [INFO] Resulting bashtest file contents:
#           $ echo 'test'
#           test
# @see https://github.com/pahaz/bashtest#typical-use-case
main() {
  dep_check &&
    bootstrap &&
    if [[ ! "${2}" =~ ^test_[[:digit:]]+/bashtests/.*\.bashtest$ ]]; then
      echo -e "\n[FAIL] Bashtest file paths must be in this format: 'test_*/bashtests/*.bashtest'." 1>&2
      return 1
    fi &&
    if [[ -f "${2}" ]]; then
      {
        echo -e "\n[INFO] Moving old bashtest file:\n       $scriptdir/$2" &&
          echo -e "       to new location:\n       $scriptdir/$2.bak" &&
          mv "$scriptdir/$2" "$scriptdir/$2.bak"
      } ||
        {
          echo -e "\n[FAIL] Failed to move old bashtest file:\n       $scriptdir/$2" 1>&2
          return 1
        }
    fi
  {
    echo -e "\n[INFO] Creating bashtest file:\n       $scriptdir/$2" &&
      printf '%s ' '$' > "$scriptdir/$2" &&
      printf "%s\n" "$1" >> "$scriptdir/$2" &&
      echo -e "\n[INFO] Evaluating command and capturing output in bashtest file..." &&
      eval "$1" >> "$scriptdir/$2" &&
      echo -e "\n[INFO] Resulting bashtest file contents:" &&
      while read -r line; do echo "       $line"; done < "$scriptdir/$2"
  } ||
    {
      echo -e "\n[FAIL] Failed to create bashtest file." 1>&2
      return 1
    }
}

# @description
#    The bootstrap funtion sets the 'scriptdir' variable for other functions to use.
#    It will also 'cd' into that directory to ensure all references to files/dirs are consistent.
# @arg $0 string The filename of the script, as called by the user
# @exitcode 0 If successful
# @exitcode 1 If failed; does not 'return 1', fatal errors 'exit 1', exiting script entirely
bootstrap() {
  {
    unset scriptdir &&
      declare -g scriptdir &&
      scriptdir="$(dirname "$(realpath "$0")")" &&
      readonly scriptdir &&
      if [[ -n "$scriptdir" ]] && [[ -d "$scriptdir" ]]; then
        cd "$scriptdir"
      else
        echo -e "\n[FAIL] Failed to detect script directory." 1>&2
        return 1
      fi
  } ||
    {
      echo -e "\n[FAIL] Failed to change directories into $0 script dir." 1>&2
      return 1
    }
}

# @description
#    This function performs a preliminary dependency check.
#    Any non-bash-builtin dep in the path should be added to this function.
# @noargs
# @exitcode 0 If all listed deps are present in $PATH
# @exitcode 1 If any listed dep is not present in $PATH; script exits immediately.
dep_check() {
  {
    for dep in mv \
      realpath \
      dirname; do
      hash $dep ||
        {
          echo -e "\n[FAIL] $dep not found in path." 1>&2
          return 1
        }
    done
  }
}

# @internal
#    This code block executes the main() function and sanitizes $2 input to only allow valid (typical) path charaters.
#    It does exclude some technically valid characters, but they are ones that aren't typically used (i.e. spaces).
#    Warning: $1 is NOT sanitized. At. All. AND, it sends the parameter to an `eval` statement. Don't be stupid!
#    There is a check in place to make this less dangerous. This script is only usable as non-root and locally.
#    If main() or any of the functions that main() calls return 1, this block prints an error and exits 1.
{
  if [[ "$EUID" == "0" ]] || [[ -n "$SUDO_USER" ]]; then
    echo -e "\n[FAIL] Script must NOT be run as root or with sudo." 1>&2
    exit 1
  fi &&
    if [[ "$REPOMAN_LOCAL_ENV" == "false" ]]; then
      echo -e "\n[FAIL] Script must ONLY be run in local environments...nice try; exiting." 1>&2
      exit 1
    fi &&
    main "${1}" "${2//[^[:alnum:]\.\/-_]/}"
} ||
  {
    echo -e "\n[FAIL] Script encountered one or more fatal errors; exiting." 1>&2
    exit 1
  }
