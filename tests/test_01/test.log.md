# TEST REPORT (TEST SCENARIO 01)

## RESULTS

```output

PLAY [decrypt-secrets] *********************************************************

TASK [decrypt-secrets | Ensuring unencrypted directory exists at /etc/repoman/secrets/encrypted/] ***
[0;33mchanged: [localhost][0m

TASK [decrypt-secrets | Ensuring decrypted copy of secrets in /etc/repoman/secrets/encrypted/ placed in /etc/repoman/secrets/unencrypted/] ***
[0;33mchanged: [localhost] => (item=/etc/repoman/secrets/encrypted/testsecret)[0m
[0;33mchanged: [localhost] => (item=/etc/repoman/secrets/encrypted/testsecret.pub)[0m

PLAY RECAP *********************************************************************
[0;33mlocalhost[0m                  : [0;32mok=2   [0m [0;33mchanged=2   [0m unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

1 items passed all tests:
   1 tests in /home/codz/projects/gitlab.com/dreamer-labs/repoman/repoman/tests/test_01/bashtests/md5sum.bashtest
1 tests in 1 items.
1 passed and 0 failed.
Test passed.
1 items passed all tests:
   1 tests in /home/codz/projects/gitlab.com/dreamer-labs/repoman/repoman/tests/test_01/bashtests/tree.bashtest
1 tests in 1 items.
1 passed and 0 failed.
Test passed.

[INFO] Test scenario 01 passed!
```

## GIT COMMIT INFO

68c9814 | feat: Add yamllint to dryrun and bump to GLF 3.5.X