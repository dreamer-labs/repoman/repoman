# entrypoint.sh (aka `repoman` inside the repoman container image)

The entrypoint for repoman

## Overview

This file is the entrypoint for repoman inside the container.
It contains the usage message for repoman.
It imports custom CA certificates (if passed in by the user).
It validates inputs passed in via docker secrets and/or env variables
It executes the subcommands listed in the usage message
It parses the REPOMAN_DEBUG message to decide whether to run in verbose/debug mode.

## Index

* [import_certs](#import_certs)
* [validate_required_inputs](#validate_required_inputs)
* [main](#main)

### import_certs

This function checks for a ca-cert directory in the repoman-data directory.
If it finds one, it loops through each file and determines the cert type.
If not plaintext (pem), it checks if it is in DER format. If so, it attempts to convert to PEM.
If successful, or if already in pem format, it checks the file extension.
`update-ca-certificates` only processes certs with filenames ending in ".crt" regardless of format.
If file does not end in ".crt", function appends it to filename.
Function then copies file ca cert staging area for inclusion in system ca trust.
Lastly, after all files are looped through, update-ca-certificates runs to include certs in CA trust.
This function runs each time "repoman <subcommand>" is called, regardless of the subcommand.

_Function has no arguments._

#### Exit codes

* **0**: If all operations attempted work.
* **1**: If certs fail to convert, copy, rename, or upload to CA trust; errors are fatal.

### validate_required_inputs

This function processes credentials and results in valid required variables.
Repoman needs a gitlab token to apply changes and an ansible vault password to encrypt secrets
These can be fed into Repoman in one of three ways:
- Credentials are placed in a default filepath (different for repoman-local vs repoman-remote)
- Credentials are passed in via environment
- Credential locations are passed in via environment
Passing credentials directly via environment variable takes precedence over designating password locations.
Explicit password location variables take precendence over implicit/default ones.
Storing these credentials in the default locations is the ultimate fallback. Default locations are:
- For `repoman-local`:
- ~/.config/repoman/ANSIBLE_VAULT_PASS
- ~/.config/repoman/GITLAB_TOKEN
- For `repoman-remote`:
- /run/secrets/ANSIBLE_VAULT_PASS
- /run/secrets/GITLAB_TOKEN
This function will first check the ANSIBLE_VAULT_PASS and GITLAB_TOKEN variables.
If populated, these variable's values are written to the ANSIBLE_VAULT_PASSWORD_FILE / GITLAB_TOKEN_FILE files.
If those file location variables are not defined or popluated, the values are written to the default file locations.
If ANSIBLE_VAULT_PASS / GITLAB_TOKEN are **NOT** set, the function checks to see if the file location vars are set.
If ANSIBLE_VAULT_PASSWORD_FILE / GITLAB_TOKEN_FILE are set, the first line of the file is written to ANSIBLE_VAULT_PASS / GITLAB_TOKEN.
If the files are blank or the variable is not set, the function will write the first line of the file at the default location to the vars instead.
The end results of this process are matching content between each creds var and its associated creds file.
- `ansible-vault` looks to the contents of the ANSIBLE_VAULT_PASSWORD_FILE for its vault password by default
- functions using vault to encrypt/decrypt take advantage of this default behavior
- `gitlabform` looks to the content of the GITLAB_TOKEN variable for its gitlab token by default
- functions using `gitlabform` do not pass the "token:" key causing it to fallback to the GITLAB_TOKEN env variable

_Function has no arguments._

#### Exit codes

* **0**: If all four vars are successfully set and both credential files are populated with data
* **1**: If any of the four vars are not populated or the credential files are empty at the end of function execution

### main

This is the entrypoint for the script.
This function calls validate_required_inputs, then import_certs.
If both pass, It parses $1 to determine the repoman subcommand to execute.
If no valid subcommand is passed, it returns the repoman_usage() message.
If REPOMAN_DEBUG is passed via the environment (not via parameter), verbose output is enabled.
Note: If used, this *should* be done *inside* the containerized environment, *after* executing `./activate`
Note: This environment variable will be ignored if repoman is being executed remotely.

