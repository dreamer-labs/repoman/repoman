#!/usr/bin/env bash
#
# @name entrypoint.sh (aka `repoman` inside the repoman container image)
# @brief The entrypoint for repoman
# @description
#    This file is the entrypoint for repoman inside the container.
#    It contains the usage message for repoman.
#    It imports custom CA certificates (if passed in by the user).
#    It validates inputs passed in via docker secrets and/or env variables
#    It executes the subcommands listed in the usage message
#    It parses the REPOMAN_DEBUG message to decide whether to run in verbose/debug mode.

# @internal
#     This line is required to make some of the globbing work properly in certain functions
shopt -s nullglob extglob

# @description
#    This function prints a usage message to users when an invalid subcommand name is passed.
# @noargs
repoman_usage() {
  echo ""
  echo "Usage: repoman <subcommand>"
  echo ""
  echo "  Subcommands:"
  echo "    decrypt-secrets: decrypt secrets in an encrypted directory and stage in a workdir directory for editing"
  echo "    encrypt-secrets: encrypt secrets in a workdir directory and stage for use by other repoman subcommands"
  echo "    deploy-configs:  deploy configuration settings using a gitlabform-formatted config.yml file"
  echo "    dryrun-deploy:   dryrun a configuration deploy using gitlabform's noop feature"
  echo "    create-project:  create a project (\$GITLAB_PROJECT_NAME) under a specified group (\$GITLAB_PARENT_ID)"
  echo "    create-subgroup: create a subgroup (\$GITLAB_SUBGROUP_NAME) under a specified group (\$GITLAB_PARENT_ID)"
  echo "    delete-project:  delete a project (\$GITLAB_PROJECT_PATH) with a specified ID (\$GITLAB_PROJECT_ID)"
  echo "    delete-subgroup: delete a subgroup (\$GITLAB_SUBGROUP_PATH) under a specified ID (\$GITLAB_SUBGROUP_ID)"
  echo ""
  echo "  Notes:"
  echo "    - repoman recommends providing an ansible-vault password (\$ANSIBLE_VAULT_PASS)"
  echo "    - as a 1st fallback, repoman checks contents of specified file (\$ANSIBLE_VAULT_PASSWORD_FILE)"
  echo "    - as a 2nd fallback, repoman checks contents of default file (~/.config/repoman/ANISBLE_VAULT_PASS)"
  echo "    - if an ansible-vault password cannot be found, one is generated, and placed in default file"
  echo "    - repoman requires providing a gitlab token (\$GITLAB_TOKEN)"
  echo "    - as a 1st fallback, repoman checks contents of specified file (\$GITLAB_TOKEN_FILE)"
  echo "    - as a 2nd fallback, repoman checks contents of default file (~/.config/repoman/GITLAB_TOKEN)"
  echo "    - if a gitlab token cannot be found, one cannot be generated, so the program exits"
  echo "    - see repoman documentation for more details on setting required and recommended variables"
  echo ""
}

# @description
#    This function checks for a ca-cert directory in the repoman-data directory.
#    If it finds one, it loops through each file and determines the cert type.
#    If not plaintext (pem), it checks if it is in DER format. If so, it attempts to convert to PEM.
#    If successful, or if already in pem format, it checks the file extension.
#    `update-ca-certificates` only processes certs with filenames ending in ".crt" regardless of format.
#    If file does not end in ".crt", function appends it to filename.
#    Function then copies file ca cert staging area for inclusion in system ca trust.
#    Lastly, after all files are looped through, update-ca-certificates runs to include certs in CA trust.
#    This function runs each time "repoman <subcommand>" is called, regardless of the subcommand.
# @noargs
# @exitcode 0 If all operations attempted work.
# @exitcode 1 If certs fail to convert, copy, rename, or upload to CA trust; errors are fatal.
import_certs() {

  if [[ -d "/etc/repoman/ca-certs/" ]]; then
    for file in /etc/repoman/ca-certs/*; do
      {
        openssl x509 -in "$file" -text -noout &> /dev/null ||
          {
            openssl x509 -in "$file" -inform der -text -noout &> /dev/null &&
              openssl x509 -inform der -in "$file" -out "$file.crt"
          }
      } ||
        {
          echo -e "\n[FAIL] CA cert file ($file) is not in pem format." 1>&2
          return 1
        }
      if [[ "${file: -4}" != ".crt" ]]; then
        mv -f "${file}" "${file}.crt" ||
          {
            echo -e "\n[FAIL] Failed to add .crt extension to CA cert file ($file)." 1>&2
            return 1
          }
      fi
      cp -f "$file" /usr/local/share/ca-certificates/repoman/ ||
        {
          echo -e "\n[FAIL] Failed to copy CA cert file ($file) to CA cert staging directory." 1>&2
          return 1
        }
    done
    update-ca-certificates &> /dev/null ||
      {
        echo -e "\n[FAIL] Failed to add custom certs to system CA trust." 1>&2
        return 1
      }
  fi

}

# @description
#    This function processes credentials and results in valid required variables.
#    Repoman needs a gitlab token to apply changes and an ansible vault password to encrypt secrets
#    These can be fed into Repoman in one of three ways:
#        - Credentials are placed in a default filepath (different for repoman-local vs repoman-remote)
#        - Credentials are passed in via environment
#        - Credential locations are passed in via environment
#    Passing credentials directly via environment variable takes precedence over designating password locations.
#    Explicit password location variables take precendence over implicit/default ones.
#    Storing these credentials in the default locations is the ultimate fallback. Default locations are:
#        - For `repoman-local`:
#            - ~/.config/repoman/ANSIBLE_VAULT_PASS
#            - ~/.config/repoman/GITLAB_TOKEN
#        - For `repoman-remote`:
#            - /run/secrets/ANSIBLE_VAULT_PASS
#            - /run/secrets/GITLAB_TOKEN
#    This function will first check the ANSIBLE_VAULT_PASS and GITLAB_TOKEN variables.
#    If populated, these variable's values are written to the ANSIBLE_VAULT_PASSWORD_FILE / GITLAB_TOKEN_FILE files.
#    If those file location variables are not defined or popluated, the values are written to the default file locations.
#    If ANSIBLE_VAULT_PASS / GITLAB_TOKEN are **NOT** set, the function checks to see if the file location vars are set.
#    If ANSIBLE_VAULT_PASSWORD_FILE / GITLAB_TOKEN_FILE are set, the first line of the file is written to ANSIBLE_VAULT_PASS / GITLAB_TOKEN.
#    If the files are blank or the variable is not set, the function will write the first line of the file at the default location to the vars instead.
#    The end results of this process are matching content between each creds var and its associated creds file.
#        - `ansible-vault` looks to the contents of the ANSIBLE_VAULT_PASSWORD_FILE for its vault password by default
#            - functions using vault to encrypt/decrypt take advantage of this default behavior
#        - `gitlabform` looks to the content of the GITLAB_TOKEN variable for its gitlab token by default
#            - functions using `gitlabform` do not pass the "token:" key causing it to fallback to the GITLAB_TOKEN env variable
# @noargs
# @exitcode 0 If all four vars are successfully set and both credential files are populated with data
# @exitcode 1 If any of the four vars are not populated or the credential files are empty at the end of function execution
validate_required_inputs() {

  # The local "fail" variable is used to ensure all checks are performed, rather than exiting after the first failure.
  local fail
  fail="false"

  if [[ -z "$GITLAB_TOKEN" ]]; then
    if [[ -f "${GITLAB_TOKEN_FILE:=/run/secrets/GITLAB_TOKEN}" ]]; then
      read -r GITLAB_TOKEN < "$GITLAB_TOKEN_FILE"
    fi
  elif [[ ! -s "${GITLAB_TOKEN_FILE:=/run/secrets/GITLAB_TOKEN}" ]]; then
    mkdir -p /run/secrets/
    printf "%s" "$GITLAB_TOKEN" > "$GITLAB_TOKEN_FILE"
  fi

  if [[ "${#GITLAB_TOKEN}" == "0" ]]; then
    echo -e "\n[FAIL] GITLAB_TOKEN variable is empty." 1>&2
    fail="true"
  fi

  if [[ ! -s "${GITLAB_TOKEN_FILE}" ]]; then
    echo -e "\n[FAIL] GITLAB_TOKEN_FILE is empty." 1>&2
    fail="true"
  fi

  { export GITLAB_TOKEN && export GITLAB_TOKEN_FILE; } || fail="true"

  if [[ -z "$ANSIBLE_VAULT_PASS" ]]; then
    if [[ -f "${ANSIBLE_VAULT_PASSWORD_FILE:=/run/secrets/ANSIBLE_VAULT_PASS}" ]]; then
      read -r ANSIBLE_VAULT_PASS < "$ANSIBLE_VAULT_PASSWORD_FILE"
    fi
  elif [[ ! -s "${ANSIBLE_VAULT_PASSWORD_FILE:=/run/secrets/ANSIBLE_VAULT_PASS}" ]]; then
    mkdir -p /run/secrets/
    printf "%s" "$ANSIBLE_VAULT_PASS" > "$ANSIBLE_VAULT_PASSWORD_FILE"
  fi

  if [[ "${#ANSIBLE_VAULT_PASS}" == "0" ]]; then
    echo -e "\n[FAIL] ANSIBLE_VAULT_PASS variable is empty." 1>&2
    fail="true"
  fi

  if [[ ! -s "${ANSIBLE_VAULT_PASSWORD_FILE}" ]]; then
    echo -e "\n[FAIL] ANSIBLE_VAULT_PASSWORD_FILE is empty." 1>&2
    fail="true"
  fi

  { export ANSIBLE_VAULT_PASS && export ANSIBLE_VAULT_PASSWORD_FILE; } || fail="true"

  # Check for failure state, and if found, print instructions before exiting; otherwise return 0.
  if [[ "$fail" == "true" ]]; then
    echo -e "\n[FAIL] Failed to setup required variables; see error messages above and/or set REPOMAN_DEBUG=true to increase message verbosity and try again; exiting."
    exit 1
  else
    return 0
  fi

}

# @description
#    This is the entrypoint for the script.
#    This function calls validate_required_inputs, then import_certs.
#    If both pass, It parses $1 to determine the repoman subcommand to execute.
#    If no valid subcommand is passed, it returns the repoman_usage() message.
#    If REPOMAN_DEBUG is passed via the environment (not via parameter), verbose output is enabled.
#    Note: If used, this *should* be done *inside* the containerized environment, *after* executing `./activate`
#    Note: This environment variable will be ignored if repoman is being executed remotely.
main() {

  local ansible_verbosity_level
  local fail="false"

  if [[ "$REPOMAN_DEBUG" == "true" ]] && [[ "$REPOMAN_LOCAL_ENV" == "true" ]]; then
    set -x
    ansible_verbosity_level="-vvvv"
  else
    set +x
    ansible_verbosity_level="-e verbosity=none"
  fi

  validate_required_inputs || return 1

  import_certs || return 1

  case "$1" in
  encrypt-secrets)
    ansible-playbook "$ansible_verbosity_level" /etc/ansible/encrypt-secrets.yml
    ;;
  decrypt-secrets)
    ansible-playbook "$ansible_verbosity_level" /etc/ansible/decrypt-secrets.yml
    ;;
  deploy-configs)
    ansible-playbook "$ansible_verbosity_level" /etc/ansible/deploy-configs.yml
    ;;
  dryrun-deploy)
    ansible-playbook "$ansible_verbosity_level" /etc/ansible/deploy-configs.yml -e "dryrun=true"
    ;;
  create-project)
    # Ensures parent group ID variable is not empty, and contains only numerical characters.
    if [[ ${#GITLAB_PARENT_ID} != "0" ]] && [[ "${GITLAB_PARENT_ID//[^0-9]/}" == "${GITLAB_PARENT_ID}" ]]; then
      export GITLAB_PARENT_ID || return 1
    else
      echo -e "\n[FAIL] Ensure GITLAB_PARENT_ID variable is set to ID of parent group and only contains numbers. Also ensure desired GITLAB_PROJECT_NAME variable is set; exiting." 1>&2
      return 1
    fi
    # Ensures the variable does not contain any common protocol strings.
    if [[ "$GITLAB_PROJECT_NAME" =~ ^(http://|https://) ]]; then
      echo -e "\n[INFO] Stripping protocol substring (${BASH_REMATCH[0]}) from gitlab project path string before using."
      echo -e "         Before: $GITLAB_PROJECT_NAME"
      local regex_match="${BASH_REMATCH[0]}"
      GITLAB_PROJECT_NAME="${GITLAB_PROJECT_NAME//$regex_match/}" || return 1
      echo -e "         After:  $GITLAB_PROJECT_NAME"
    fi
    # Ensures the variable does not contain any trailing slashes.
    until [[ "${GITLAB_PROJECT_NAME: -1}" != "/" ]]; do
      echo -e "\n[INFO] Stripping trailing slash substring from gitlab project name string before using."
      echo -e "         Before: $GITLAB_PROJECT_NAME"
      GITLAB_PROJECT_NAME="${GITLAB_PROJECT_NAME%/}" || return 1
      echo -e "         After:  $GITLAB_PROJECT_NAME"
    done
    # Ensures the variable does not contain any leading slashes.
    until [[ "${GITLAB_PROJECT_NAME:0:1}" != "/" ]]; do
      echo -e "\n[INFO] Stripping leading slash substring from gitlab project name string before using."
      echo -e "         Before: $GITLAB_PROJECT_NAME"
      GITLAB_PROJECT_NAME="${GITLAB_PROJECT_NAME#/}" || return 1
      echo -e "         After:  $GITLAB_PROJECT_NAME"
    done
    if [[ "$GITLAB_PROJECT_NAME" != "${GITLAB_PROJECT_NAME##*/}" ]]; then
      echo -e "\n[INFO] Stripping leading group names from gitlab project name string before using."
      echo -e "         Before: $GITLAB_PROJECT_NAME"
      GITLAB_PROJECT_NAME="${GITLAB_PROJECT_NAME##*/}" || return 1
      echo -e "         After:  $GITLAB_PROJECT_NAME"
    fi
    # Ensures the variable contains at least one character and no longer contains any remaining parent group names or leading/trailing slashes, (after removing those things above).
    if [[ "${#GITLAB_PROJECT_NAME}" != "0" ]] && [[ "${GITLAB_PROJECT_NAME//\//}" == "${GITLAB_PROJECT_NAME}" ]]; then
      export GITLAB_PROJECT_NAME || return 1
    else
      echo -e "\n[FAIL] Ensure GITLAB_PROJECT_NAME variable is set to partial/relative path of project only, with no trailing or leading slash; exiting." 1>&2
      echo -e "         DO NOT set to something like this: top-level-org-path-here/parent-suborg-path-here/my-project-name-here/" 1>&2
      echo -e "         DO NOT set to something like this: /top-level-org-path-here/parent-suborg-path-here/my-project-name-here/" 1>&2
      echo -e "         DO NOT set to something like this: https://gitlab.com/top-level-org-path-here/parent-suborg-path-here/my-project-name-here/" 1>&2
      echo -e "         DO NOT set to something like this: my-project-name-here/" 1>&2
      echo -e "         DO set to something like this: my-project-name-here" 1>&2
      return 1
    fi
    # Regex for subgroup name restrictions is complex and changes regularly, so only common issues like misplaced slashes and protocol strings are checked for before using the var below.
    # Subgroup name restrictions can be found in Gitlab docs, (link still valid as of Gitlab v15.5): https://docs.gitlab.com/ee/user/reserved_names.html
    ansible-playbook "$ansible_verbosity_level" /etc/ansible/create-project.yml -e "gitlab_parent_id=$GITLAB_PARENT_ID" -e "gitlab_project_name=$GITLAB_PROJECT_NAME"
    ;;
  create-subgroup)
    # Ensures parent group ID variable is not empty, and contains only numerical characters.
    if [[ ${#GITLAB_PARENT_ID} != "0" ]] && [[ "${GITLAB_PARENT_ID//[^0-9]/}" == "${GITLAB_PARENT_ID}" ]]; then
      export GITLAB_PARENT_ID || return 1
    else
      echo -e "\n[FAIL] Ensure GITLAB_PARENT_ID variable is set to ID of parent group and only contains numbers; exiting." 1>&2
      return 1
    fi
    # Ensures the variable does not contain any common protocol strings.
    if [[ "$GITLAB_SUBGROUP_NAME" =~ ^(http://|https://) ]]; then
      echo -e "\n[INFO] Stripping protocol substring (${BASH_REMATCH[0]}) from gitlab subgroup path string before using."
      echo -e "         Before: $GITLAB_SUBGROUP_NAME"
      local regex_match="${BASH_REMATCH[0]}"
      GITLAB_SUBGROUP_NAME="${GITLAB_SUBGROUP_NAME//$regex_match/}" || return 1
      echo -e "         After:  $GITLAB_SUBGROUP_NAME"
    fi
    # Ensures the variable does not contain any trailing slashes.
    until [[ "${GITLAB_SUBGROUP_NAME: -1}" != "/" ]]; do
      echo -e "\n[INFO] Stripping trailing slash substring from gitlab subgroup name string before using."
      echo -e "         Before: $GITLAB_SUBGROUP_NAME"
      GITLAB_SUBGROUP_NAME="${GITLAB_SUBGROUP_NAME%/}" || return 1
      echo -e "         After:  $GITLAB_SUBGROUP_NAME"
    done
    # Ensures the variable does not contain any leading slashes.
    until [[ "${GITLAB_SUBGROUP_NAME:0:1}" != "/" ]]; do
      echo -e "\n[INFO] Stripping leading slash substring from gitlab subgroup name string before using."
      echo -e "         Before: $GITLAB_SUBGROUP_NAME"
      GITLAB_SUBGROUP_NAME="${GITLAB_SUBGROUP_NAME#/}" || return 1
      echo -e "         After:  $GITLAB_SUBGROUP_NAME"
    done
    if [[ "$GITLAB_SUBGROUP_NAME" != "${GITLAB_SUBGROUP_NAME##*/}" ]]; then
      echo -e "\n[INFO] Stripping leading group names from gitlab subgroup name string before using."
      echo -e "         Before: $GITLAB_SUBGROUP_NAME"
      GITLAB_SUBGROUP_NAME="${GITLAB_SUBGROUP_NAME##*/}" || return 1
      echo -e "         After:  $GITLAB_SUBGROUP_NAME"
    fi
    # Ensures the variable contains at least one character and no longer contains any remaining parent group names or leading/trailing slashes, (after removing those things above).
    if [[ "${#GITLAB_SUBGROUP_NAME}" != "0" ]] && [[ "${GITLAB_SUBGROUP_NAME//\//}" == "${GITLAB_SUBGROUP_NAME}" ]]; then
      export GITLAB_SUBGROUP_NAME || return 1
    else
      echo -e "\n[FAIL] Ensure GITLAB_SUBGROUP_NAME variable is set to partial/relative path of subgroup only, with no trailing or leading slash; exiting." 1>&2
      echo -e "         DO NOT set to something like this: top-level-org-path-here/parent-suborg-path-here/my-group-name-here/" 1>&2
      echo -e "         DO NOT set to something like this: /top-level-org-path-here/parent-suborg-path-here/my-group-name-here/" 1>&2
      echo -e "         DO NOT set to something like this: https://gitlab.com/top-level-org-path-here/parent-suborg-path-here/my-group-name-here/" 1>&2
      echo -e "         DO NOT set to something like this: my-group-name-here/" 1>&2
      echo -e "         DO set to something like this: my-group-name-here" 1>&2
      return 1
    fi
    # Regex for subgroup name restrictions is complex and changes regularly, so only common issues like misplaced slashes and protocol strings are checked for before using the var below.
    # Subgroup name restrictions can be found in Gitlab docs, (link still valid as of Gitlab v15.5): https://docs.gitlab.com/ee/user/reserved_names.html
    ansible-playbook "$ansible_verbosity_level" /etc/ansible/create-subgroup.yml -e "gitlab_parent_id=$GITLAB_PARENT_ID" -e "gitlab_subgroup_name=$GITLAB_SUBGROUP_NAME"
    ;;
  delete-project)
    # Ensures project ID variable is not empty, and contains only numerical characters.
    if [[ "${#GITLAB_PROJECT_ID}" != "0" ]] && [[ "${GITLAB_PROJECT_ID//[^0-9]/}" == "${GITLAB_PROJECT_ID}" ]]; then
      export GITLAB_PROJECT_ID || return 1
    else
      echo -e "\n[FAIL] Ensure GITLAB_PROJECT_ID variable is set to ID of project and only contains numbers; exiting." 1>&2
      return 1
    fi
    # Ensures the variable does not contain any common protocol strings.
    if [[ "$GITLAB_PROJECT_PATH" =~ ^(http://|https://) ]]; then
      echo -e "\n[INFO] Stripping protocol substring (${BASH_REMATCH[0]}) from gitlab project path string before using."
      echo -e "         Before: $GITLAB_PROJECT_PATH"
      local regex_match="${BASH_REMATCH[0]}"
      GITLAB_PROJECT_PATH="${GITLAB_PROJECT_PATH//$regex_match/}" || return 1
      echo -e "         After:  $GITLAB_PROJECT_PATH"
    fi
    # Ensures the variable does not contain any trailing slashes.
    until [[ "${GITLAB_PROJECT_PATH: -1}" != "/" ]]; do
      echo -e "\n[INFO] Stripping trailing slash substring from gitlab project path string before using."
      echo -e "         Before: $GITLAB_PROJECT_PATH"
      GITLAB_PROJECT_PATH="${GITLAB_PROJECT_PATH%/}" || return 1
      echo -e "         After:  $GITLAB_PROJECT_PATH"
    done
    # Ensures the variable does not contain any leading slashes.
    until [[ "${GITLAB_PROJECT_PATH:0:1}" != "/" ]]; do
      echo -e "\n[INFO] Stripping leading slash substring from gitlab project path string before using."
      echo -e "         Before: $GITLAB_PROJECT_PATH"
      GITLAB_PROJECT_PATH="${GITLAB_PROJECT_PATH#/}" || return 1
      echo -e "         After:  $GITLAB_PROJECT_PATH"
    done
    # Ensures the variable contains at least one character and still contains at least one slash, (after removing any trailing or leading ones).
    if [[ "${#GITLAB_PROJECT_PATH}" != "0" ]] && [[ "${GITLAB_PROJECT_PATH//\//}" != "${GITLAB_PROJECT_PATH}" ]]; then
      export GITLAB_PROJECT_PATH || return 1
    else
      echo -e "\n[FAIL] Ensure GITLAB_PROJECT_PATH variable is set to full path of project, with no leading or trailing slash; exiting." 1>&2
      echo -e "         DO NOT set to something like this: my-group-name-here/" 1>&2
      echo -e "         DO NOT set to something like this: my-group-name-here" 1>&2
      echo -e "         DO NOT set to something like this: /top-level-org-path-here/parent-suborg-path-here/my-group-name-here" 1>&2
      echo -e "         DO NOT set to something like this: /top-level-org-path-here/parent-suborg-path-here/my-group-name-here/" 1>&2
      echo -e "         DO NOT set to something like this: https://gitlab.com/top-level-org-path-here/parent-suborg-path-here/my-group-name-here/" 1>&2
      echo -e "         DO set to something like this: top-level-org-path-here/parent-suborg-path-here/my-group-name-here" 1>&2
      return 1
    fi
    # Regex for project path restrictions is complex and changes regularly, so only common issues like misplaced slashes and protocol strings are checked for before using the var below.
    # Project path restrictions can be found in Gitlab docs, (link still valid as of Gitlab v15.5): https://docs.gitlab.com/ee/user/reserved_names.html
    ansible-playbook "$ansible_verbosity_level" /etc/ansible/delete-project.yml -e "gitlab_project_id=$GITLAB_PROJECT_ID" -e "gitlab_project_path=$GITLAB_PROJECT_PATH"
    ;;
  delete-subgroup)
    # Ensures subgroup ID variable is not empty, and contains only numerical characters.
    if [[ "${#GITLAB_SUBGROUP_ID}" != "0" ]] && [[ "${GITLAB_SUBGROUP_ID//[^0-9]/}" == "${GITLAB_SUBGROUP_ID}" ]]; then
      export GITLAB_SUBGROUP_ID || return 1
    else
      echo -e "\n[FAIL] Ensure GITLAB_SUBGROUP_ID variable is set to ID of subgroup and only contains numbers; exiting." 1>&2
      return 1
    fi
    # Ensures the variable does not contain any common protocol strings.
    if [[ "$GITLAB_SUBGROUP_PATH" =~ ^(http://|https://) ]]; then
      echo -e "\n[INFO] Stripping protocol substring (${BASH_REMATCH[0]}) from gitlab subgroup path string before using."
      echo -e "         Before: $GITLAB_SUBGROUP_PATH"
      local regex_match="${BASH_REMATCH[0]}"
      GITLAB_SUBGROUP_PATH="${GITLAB_SUBGROUP_PATH//$regex_match/}" || return 1
      echo -e "         After:  $GITLAB_SUBGROUP_PATH"
    fi
    # Ensures the variable does not contain any trailing slashes.
    until [[ "${GITLAB_SUBGROUP_PATH: -1}" != "/" ]]; do
      echo -e "\n[INFO] Stripping trailing slash substring from gitlab subgroup path string before using."
      echo -e "         Before: $GITLAB_SUBGROUP_PATH"
      GITLAB_SUBGROUP_PATH="${GITLAB_SUBGROUP_PATH%/}" || return 1
      echo -e "         After:  $GITLAB_SUBGROUP_PATH"
    done
    # Ensures the variable does not contain any leading slashes.
    until [[ "${GITLAB_SUBGROUP_PATH:0:1}" != "/" ]]; do
      echo -e "\n[INFO] Stripping leading slash substring from gitlab subgroup path string before using."
      echo -e "         Before: $GITLAB_SUBGROUP_PATH"
      GITLAB_SUBGROUP_PATH="${GITLAB_SUBGROUP_PATH#/}" || return 1
      echo -e "         After:  $GITLAB_SUBGROUP_PATH"
    done
    if [[ "$GITLAB_SUBGROUP_PATH" != "${GITLAB_SUBGROUP_PATH##*/}" ]]; then
      echo -e "\n[INFO] Stripping leading group names from gitlab subgroup path string before using."
      echo -e "         Before: $GITLAB_SUBGROUP_PATH"
      GITLAB_SUBGROUP_PATH="${GITLAB_SUBGROUP_PATH##*/}" || return 1
      echo -e "         After:  $GITLAB_SUBGROUP_PATH"
    fi
    # Ensures the variable contains at least one character and no longer contains any remaining parent group names or leading/trailing slashes, (after removing those things above).
    if [[ "${#GITLAB_SUBGROUP_PATH}" != "0" ]] && [[ "${GITLAB_SUBGROUP_PATH//\//}" == "${GITLAB_SUBGROUP_PATH}" ]]; then
      export GITLAB_SUBGROUP_PATH || return 1
    else
      echo -e "\n[FAIL] Ensure GITLAB_SUBGROUP_PATH variable is set to partial/relative path of subgroup only, with no trailing or leading slash; exiting." 1>&2
      echo -e "         DO NOT set to something like this: top-level-org-path-here/parent-suborg-path-here/my-group-name-here/" 1>&2
      echo -e "         DO NOT set to something like this: /top-level-org-path-here/parent-suborg-path-here/my-group-name-here/" 1>&2
      echo -e "         DO NOT set to something like this: https://gitlab.com/top-level-org-path-here/parent-suborg-path-here/my-group-name-here/" 1>&2
      echo -e "         DO NOT set to something like this: my-group-name-here/" 1>&2
      echo -e "         DO set to something like this: my-group-name-here" 1>&2
      return 1
    fi
    # Regex for subgroup path restrictions is complex and changes regularly, so only common issues like the addition of slashes are checked for before using the var below.
    # Subgroup path restrictions can be found in Gitlab docs, (link still valid as of Gitlab v15.5): https://docs.gitlab.com/ee/user/reserved_names.html
    ansible-playbook "$ansible_verbosity_level" /etc/ansible/delete-subgroup.yml -e "gitlab_subgroup_id=$GITLAB_SUBGROUP_ID" -e "gitlab_subgroup_path=$GITLAB_SUBGROUP_PATH"
    ;;
  *)
    echo -e "\n[FAIL] Failed to pass a valid subcommand to repoman; exiting." 1>&2
    return 1
    ;;
  esac

}

# @internal
#     This execution block determines if REPOMAN_DEBUG & REPOMAN_LOCAL_ENV are set to true.
#     If they both are, it toggles on bash debugging.
#     This is **NOT** toggled on in remotely executed environments because it will expose secrets in pipeline output.
#     REPOMAN_LOCAL_ENV is set by the Dockerfile.
#     REPOMAN_DEBUG is passed by the user over the command line before executing `repoman <subcommand>`.
#     Note: this *should* be done *inside* the containerized environment, *after* executing `./launch-repoman`
#     The test suite automatically passes this variable to the test environments it spins up.
#     After determining whether or not to set debugging, this code block executes main().
#     It also sanitizes $1 to allow only "[[:alpha:]]" and "-" since subcommand names only include those characters.
main "${1//[^[:alpha:]-]/}" ||
  {
    repoman_usage
    echo -e "\n[FAIL] Repoman failed with one or more fatal errors; please read error and usage messages and try again.\n" 1>&2
    exit 1
  }
