# repoman-completion

This is a bash completion script for `repoman` inside the container

## Overview

This function provides bash completion options to the user

## Index

* [_repoman_completion](#_repoman_completion)

### _repoman_completion

This function provides bash completion options to the user

_Function has no arguments._

